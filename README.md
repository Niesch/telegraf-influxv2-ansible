# Telegraf for Influx v2
This is pretty heavily inspired from [github.com/rossmcdonald/telegraf](https://github.com/rossmcdonald/telegraf), but reworked to work with Influx v2, in which things like modules can be configured from Influx itself.

The authentication token and configuration URL are set in `/etc/default/telegraf`, which systemd passes on to telegraf itself.

